//Lautaro Zarandon, Facultad de Ingenieria, Universidad de Mendoza, Computacion.
#include "persona.h"
#include <iostream>
using namespace std;

int main(int argc, char * const argv[])
{
Persona persona;
    persona.ingresar();
    persona.mostrar();
    cout<<"(constructor sobrecargado:) \n";
    Persona p1(20320493, "Garcia", "Jose");//sobrecargado
    p1.mostrar();

    return 0;
}
