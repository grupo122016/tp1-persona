#include "persona.h"
#include <iostream>

using namespace std;
Persona::Persona()
{
dni=0;
nombre="";
apellido="";
}

Persona::Persona (int dni, string apellido, string nombre)
{
    this->dni = dni;
    this->apellido = apellido;
    this->nombre = nombre;
}

void Persona::ingresar()
{
int x;
string y;
cout<< "Ingrese su numero de documento: \n";
cin>> x;
this->setDni(x);
cout<< "Ingrese su nombre: \n";
cin>> y;
this->setNombre(y);
cout<< "Ingrese su apellido: \n";
cin>> y;
this->setApellido(y);
}

void Persona::mostrar()
{
cout<< "("<<this->getDni()<<" dni,"<<this->getApellido()<<" apellido,"<<this->getNombre()<<" nombre)" <<endl;
}

void Persona::setDni(int dni){
this->dni=dni;
}

int Persona::getDni(){
return this->dni;
}

void Persona::setNombre(string nombre){
this->nombre=nombre;
}

string Persona::getNombre(){
return this->nombre;}

void Persona::setApellido(string apellido){
this->apellido=apellido;
}

string Persona::getApellido(){
return this ->apellido;}
